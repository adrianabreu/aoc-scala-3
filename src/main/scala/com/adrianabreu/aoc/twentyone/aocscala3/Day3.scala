package com.adrianabreu.aoc.twentyone.aocscala3

import com.adrianabreu.aoc.utils.Day
import scala.collection.MapView

object Day3 extends Day[Long]:

 override def part1(input: List[String]): Long =
  val values = findGammaAndEpsilon(input)
  values._1 * values._2

 override def part2(input: List[String]): Long =
    val values = findOxigenAndCo2(input)
    values._1 * values._2


 private def getMatrix(input: List[String]): List[List[String | Null]] =
    input
    .map(s => s.split("").nn.toList).transpose

 private def getColumnFrequency(input: List[String]): List[MapView[String | Null, Int]] =
    getMatrix(input)
    .map(column => column.groupBy(identity).mapValues(_.size))

 private def findGammaAndEpsilon(input: List[String]): (Int, Int) =
    val columnFrequency = getColumnFrequency(input)
    val binaryMeasures = columnFrequency
    .map(column => {
        (column.maxBy(c => c._2)._1, column.minBy(c => c._2)._1)
    }).foldLeft(("",""))((prev, curr) => {
        (prev._1.nn + curr._1.nn, prev._2 + curr._2)
    })

    binaryMeasures match
        case (gamma, epsilon) => (Integer.parseInt(gamma, 2), Integer.parseInt(epsilon, 2))

 private def findOxigenAndCo2(input: List[String]): (Int, Int) =


    @tailrec
    def loop(input: List[String], position: Int, criteria: (Int,Int) => Boolean): Int =
        input match
            case Nil => throw new IndexOutOfBoundsException
            case head :: Nil => Integer.parseInt(head, 2)
            case _ => {
              val mostCommonValue = getColumnFrequency(input)(position).reduce((prev, curr) => {
                if (prev._2 > curr._2) prev
                else if (prev._2 == curr._2) ("1", 1)
                else curr
              })._1

                val remainingRows = input.filter(row => {
                  criteria(Integer.parseInt(row(position).toString,2), Integer.parseInt(mostCommonValue, 2))
                })
                loop(remainingRows, position + 1, criteria)
            }

    (loop(input, 0, (a,b) => a ==b), loop(input, 0, (a,b) => a != b))

 override val day = 3



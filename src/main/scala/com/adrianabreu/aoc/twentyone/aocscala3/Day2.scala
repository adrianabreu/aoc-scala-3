package com.adrianabreu.aoc.twentyone.aocscala3

import com.adrianabreu.aoc.utils.Day

object Day2 extends Day[Long]:

 override def part1(input: List[String]): Long = 
  val pos = calculateCoords(input, calculateWithoutAim)
  pos.x * pos.y

 override def part2(input: List[String]): Long = 
  val pos = calculateCoords(input, calculateWithAim)
  pos.x * pos.y

 private def calculateCoords(input: List[String], calculateFunc: (Position, (String, Int)) => Position): Position =
    input
    .map(s => s.split(' '))
    .map{ case  Array(v1, v2) => (v1, v2.toInt)}
    .foldLeft(new Position(0,0)): 
        case (acc, value) => calculateFunc(acc, value)
        

 private def calculateWithoutAim(acc: Position, value: (String, Int)): Position =
    value._1 match
        case "forward" => new Position(acc.x + value._2, acc.y)
        case "down" => new Position(acc.x, acc.y + value._2)
        case "up" => new Position(acc.x, acc.y - value._2)

 private def calculateWithAim(acc: Position, value: (String, Int)): Position =
    value._1 match
        case "forward" => new Position(acc.x + value._2, acc.y + acc.aim * value._2)
        case "down" => new Position(acc.x, acc.y + value._2, acc.aim + value._2)
        case "up" => new Position(acc.x, acc.y - value._2, acc.aim - value._2)


 override val day = 2
  
  
case class Position(x: Int, y: Int, aim: Int = 0)

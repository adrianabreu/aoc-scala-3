package com.adrianabreu.aoc.twentyone.aocscala3

import com.adrianabreu.aoc.utils.Day

object Day1 extends Day[Int]:

 override def part1(input: List[String]): Int = 
  input.map(_.toInt).sliding(2).count{ case a :: b :: Nil => a < b}

 override def part2(input: List[String]): Int =
  input.map(_.toInt).sliding(3, 1).map(_.sum).toList.sliding(2).count{ case List(a,b) => a < b}
  

 override val day = 1
  
  


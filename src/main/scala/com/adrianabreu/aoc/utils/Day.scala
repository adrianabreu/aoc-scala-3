package com.adrianabreu.aoc.utils

import scala.io.Source

trait Day[T] {
    val day: Int 
    def part1(input: List[String]): T
    def part2(input: List[String]): T

    def main(args: Array[String]): Unit =
        val input1 = Source.fromResource(s"day${day}_input.txt").getLines.toList

        println(part1(input1))
        println(part2(input1))
}
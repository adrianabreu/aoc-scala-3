package com.adrianabreu.aoc.twentyone.aocscala3

import scala.io.Source


export org.scalacheck.Arbitrary
export org.scalacheck.Cogen
export org.scalacheck.Gen

trait TestSuite extends munit.DisciplineSuite, Expectations:
 lazy val day: Int
 lazy val testInput = Source.fromResource(s"day${day}_input.txt").getLines().toList

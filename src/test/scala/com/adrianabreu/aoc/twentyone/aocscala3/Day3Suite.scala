package com.adrianabreu.aoc.twentyone.aocscala3

final class Day3Suite extends TestSuite:
  override lazy val day: Int = 3
  
  test("part 3 should get the final position properly"):
    val actual = Day3.part1(testInput)

    expectEquals(actual, 198L)

  test("part 3 should final position including aim") {
    val actual = Day3.part2(testInput)

    expectEquals(actual, 230L)
  }
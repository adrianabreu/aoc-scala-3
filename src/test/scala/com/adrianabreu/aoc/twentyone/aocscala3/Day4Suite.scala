package com.adrianabreu.aoc.twentyone.aocscala3

final class Day4Suite extends TestSuite:
  override lazy val day: Int = 4
  
  test("part 1 should get the proper winning board score"):
    val actual = Day4.part1(testInput)

    expectEquals(actual, 4512L)

  test("part 2 should final position including aim") {
    val actual = Day4.part2(testInput)

    expectEquals(actual, 230L)
  }
package com.adrianabreu.aoc.twentyone.aocscala3

final class Day2Suite extends TestSuite:
  override lazy val day: Int = 2
  
  test("part 2 should get the final position properly"):
    val actual = Day2.part1(testInput)

    expectEquals(actual, 150L)

    test("part 2 should final position including aim") {
    val actual = Day2.part2(testInput)

    expectEquals(actual, 900L)
  }
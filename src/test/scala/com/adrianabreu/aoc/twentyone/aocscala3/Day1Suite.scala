package com.adrianabreu.aoc.twentyone.aocscala3

import scala.io.Source

final class Day1Suite extends TestSuite:
  override lazy val day: Int = 1
  
  test("part 1 should calculate consecutive increases properly"):
    val actual = Day1.part1(testInput)

    expectEquals(actual, 7)

    test("part 2 should calculate consecutive three window sm¡um increases properly") {
    val actual = Day1.part2(testInput)

    expectEquals(actual, 5)
  }